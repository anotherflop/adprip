function parseElecteurs(electeursCSV) {
	var electeursJson = []
	var headersString = 'dpt_code,dept_nom,nb_electeurs'
	var headers = headersString.split(',')

	electeursCSV = electeursCSV.replace(/['"]+/g, '')
	electeursCSV = electeursCSV.split('\n')

	electeursCSV.forEach(function(electeur) {
		var data = electeur.split(',')
		if (data.length > 1) {
			var obj = {}
			for (var j = 0; j < data.length; j++) {
				obj[headers[j].trim()] = data[j].trim()
			}
			obj.nb_electeurs = parseInt(obj.nb_electeurs)
			electeursJson.push(obj)
		}
	})
	electeursJson.shift()
	return electeursJson
}
